/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:
function userDetails() {
    const fullName = prompt('Enter your fullname');
    const age = prompt('Enter your age');
    const location = prompt('Enter your location');
    alert('Thank you for your input.');

    console.log('Hello, ' + fullName);
    console.log('You are ' + age + ' years old.');
    console.log('You live in ' + location);
}

userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
function topAct() {
    console.log(
        "1. The Beatles\n2. Metallica\n3. The Eagles\n4.L'arc~en~Ciel\n5. Eraserheads"
    );
}

topAct();

function alternativeFunction() {
    const kpop = ['Mamamoo', 'Purple Kiss', 'Apink', 'BTS', '2ne1'];
    console.log(kpop);
}

alternativeFunction();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
function top5FavouriteMovies() {
    console.log('1. The Godfather\nRotten Tomatoes Rating: 97%');
    console.log('2. The Godfather, Part II\nRotten Tomatoes Rating: 96%');
    console.log('3. Shawnshank Redemption\nRotten Tomatoes Rating: 91%');
    console.log('4. To Kill A Mockingbird\nRotten Tomatoes Rating: 93%');
    console.log('5. Psycho\nRotten Tomatoes Rating: 96%');
}

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printUsers = function() {
    alert('Hi! Please add the names of your friends.');
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");

    console.log('You are friends with:');
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};

printUsers();